<?php

namespace common\classes;

interface BlockInterface
{
    public static function renderBlock($block_id);
}