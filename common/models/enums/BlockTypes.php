<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

class BlockTypes extends BaseEnum
{

    const HEADER = 'Header';
    const CONTENT_4 = 'Content4';
    const CONTENT_5 = 'Content5';
    const CONTENT_5_REVERSE = 'Content5Reverse';
    const CONTENT_11 = 'Content11';
    const CONTENT_12_REVERSE = 'Content12Reverse';
    const CONTENT_13 = 'Content13';
    const CONTENT_14 = 'Content14';
    const CONTENT_16 = 'Content16';
    const EMPLOYED = 'Employed';
    const TESTIMONIALS = 'Testimonials';
    const FEATURES = 'Features';
    const PRICING = 'Pricing';
    const FOOTER = 'Footer';
    const COUNTER = 'Counter';

    /**
     * @var array
     */
    public static $list = [
        self::HEADER => 'Header',
        self::CONTENT_4 => 'Content4',
        self:: CONTENT_5 => 'Content5',
        self:: CONTENT_5_REVERSE => 'Content5Reverse',
        self:: CONTENT_11 => 'Content11',
        self:: CONTENT_12_REVERSE => 'Content12Reverse',
        self:: CONTENT_13 => 'Content13',
        self:: CONTENT_14 => 'Content14',
        self:: CONTENT_16 => 'Content16',
        self:: EMPLOYED => 'Employed',
        self:: TESTIMONIALS => 'Testimonials',
        self:: FEATURES => 'Features',
        self:: PRICING => 'Pricing',
        self:: FOOTER => 'Footer',
        self:: COUNTER => 'Counter',
    ];
}