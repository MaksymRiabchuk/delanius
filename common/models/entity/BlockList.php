<?php

namespace common\models\entity;

use common\models\enums\BlockTypes;
use Yii;

/**
 * This is the model class for table "landing_block_list".
 *
 * @property int $id
 * @property int $position
 * @property string $type
 * @property string $name
 */
class BlockList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'landing_block_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'name', ], 'required'],
            [['position',], 'integer'],
            ['position', 'default', 'value' => '1'],
            [['type', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'position' => Yii::t('app', 'Position'),
            'type' => Yii::t('app', 'Type'),
            'name' => Yii::t('app', 'Name'),
        ];
    }
    public function beforeSave($insert)
    {

        if ($insert) {
            $maxPosition = BlockList::find()->max('position');
            $this->position = $maxPosition + 1;
        }
        return parent::beforeSave($insert);
    }
    public function moveUp()
    {
        /* @var $classDown BlockList */
        $classDown = BlockList::find()->where(['<', 'position', $this->position])->orderBy(['position'=>SORT_ASC])->one();
        if ($this && $classDown) {
            $p = $this->position;
            $this->position = $classDown->position;
            $classDown->position = $p;
            $classDown->save();
            $this->save();
        }
    }
    public function moveDown()
    {
        /* @var $classDown BlockList */
        $classDown = BlockList::find()->where(['>', 'position', $this->position])->orderBy(['position'=>SORT_ASC])->one();
        if ($this && $classDown) {
            $p = $this->position;
            $this->position = $classDown->position;
            $classDown->position = $p;
            $classDown->save();
            $this->save();
        }
    }
}
