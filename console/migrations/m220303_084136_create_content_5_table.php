<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%content_5}}`.
 */
class m220303_084136_create_content_5_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%content5}}', [
            'id' => $this->primaryKey(),
            'image'=>$this->string()->notNull(),
            'description'=>$this->text()->notNull(),
            'name'=>$this->string()->notNull(),
            'block_id'=>$this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%content5}}');
    }
}
