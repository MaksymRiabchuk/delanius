<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%block_list}}`.
 */
class m220303_085127_create_block_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%block_list}}', [
            'id' => $this->primaryKey(),
            'position'=> $this->smallInteger()->notNull(),
            'type'=> $this->string()->notNull(),
            'name'=> $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%block_list}}');
    }
}
