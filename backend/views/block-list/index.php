<?php

use common\models\entity\BlockList;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\BlockListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Block Lists');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-list-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Block List'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'type',
            [
                'attribute'=>'position',
                'format'=>'raw',
                'filter'=>false,
                'value'=>function($model){
                    /* @var $model BlockList */
                    return Html::a('<span class="fa fa-arrow-up" style="color: green"></span>',
                            ['/block-list/move-up','id'=>$model->id]).' '.Html::a(
                            '<span class="fa fa-arrow-down" style="color: red"></span>',
                            ['/block-list/move-down','id'=>$model->id]);
                }
            ],

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'Дії',
                'template'=>'{update} {delete} {moveUp} {moveDown}',
            ],
        ],
    ]); ?>


</div>
